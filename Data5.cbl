       IDENTIFICATION DIVISION. 
       PROGRAM-ID. DATA5.
       AUTHOR. NUTHJAREE.
       DATA DIVISION. 
       WORKING-STORAGE SECTION.
       01 GRADE-DATA PIC X(90) VALUE "62160137NUTHJAREE    886345593A
      -    " 886352593A 886342193A 886478593A 886481592A 886491591A ". 

       01  GRADE REDEFINES GRADE-DATA .
         03 STD-ID         PIC   9(8).
         03 STD-NAME       PIC   X(16).

         03   SUB1.
           05 SUB-CODE1      PIC   9(8).
           05 SUB-UNIT1      PIC   9.
           05 SUB-GRADE1     PIC   X(2).

         03   SUB2.
           05 SUB-CODE2      PIC   9(8).
           05 SUB-UNIT2      PIC   9.
           05 SUB-GRADE2     PIC   X(2).

         03   SUB3.
           05 SUB-CODE3      PIC   9(8).
           05 SUB-UNIT3      PIC   9.
           05 SUB-GRADE3     PIC   X(2).

         03   SUB4.
           05 SUB-CODE4      PIC   9(8).
           05 SUB-UNIT4      PIC   9.
           05 SUB-GRADE4     PIC   X(2).

         03   SUB5.
           05 SUB-CODE5      PIC   9(8).
           05 SUB-UNIT5      PIC   9.
           05 SUB-GRADE5     PIC   X(2).

         03   SUB6.
           05 SUB-CODE6      PIC   9(8).
           05 SUB-UNIT6      PIC   9.
           05 SUB-GRADE6     PIC   X(2).
       
       66  STUDENT-ID  RENAMES STD-ID.
       66  STUDENT-INFO   RENAMES STD-ID THRU  SUB-UNIT1 .

       01  STUDENT-CODE REDEFINES GRADE-DATA  .
           05 STU-YEAR       PIC   9(2).
           05 FILLER         PIC   X(6).
           05 STU-SHORT-NAME PIC   X(3).
       
       PROCEDURE DIVISION .
       Begin.
           DISPLAY GRADE 

           DISPLAY "SUJECT 1"
           DISPLAY "CODE 1 : " SUB-CODE1 
           DISPLAY "UNIT : " SUB-UNIT1  
           DISPLAY "GRADE : "  SUB-GRADE1 
           DISPLAY "------------------------"
           
           DISPLAY "SUJECT 2"
           DISPLAY "CODE 2 : " SUB-CODE2
           DISPLAY "UNIT : " SUB-UNIT2 
           DISPLAY "GRADE : "  SUB-GRADE2 
           DISPLAY "------------------------"

           DISPLAY "SUJECT 3"
           DISPLAY "CODE 3 : " SUB-CODE3
           DISPLAY "UNIT : " SUB-UNIT3
           DISPLAY "GRADE : "  SUB-GRADE3
           DISPLAY "------------------------"
           
           DISPLAY "SUJECT 4"
           DISPLAY "CODE 4 : " SUB-CODE4
           DISPLAY "UNIT : " SUB-UNIT4
           DISPLAY "GRADE : "  SUB-GRADE4
           DISPLAY "------------------------"
           
           DISPLAY "SUJECT 5"
           DISPLAY "CODE 5 : " SUB-CODE5 
           DISPLAY "UNIT : " SUB-UNIT5
           DISPLAY "GRADE : "  SUB-GRADE5
           DISPLAY "------------------------"
           
           DISPLAY "SUJECT 6"
           DISPLAY "CODE 6 : " SUB-CODE6 
           DISPLAY "UNIT : " SUB-UNIT6
           DISPLAY "GRADE : "  SUB-GRADE6 
           DISPLAY "------------------------"
           DISPLAY STUDENT-ID 
           DISPLAY STUDENT-INFO 
           DISPLAY STU-YEAR STU-SHORT-NAME 
           .